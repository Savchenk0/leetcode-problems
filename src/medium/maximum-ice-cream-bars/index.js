const maxBars = function (costs, coins) {
    let spent = 0
    const sortedCosts = costs.sort((a, b) => a - b)
    for (let i = 0; i < sortedCosts.length; i++) {
        spent += sortedCosts[i]
        if (spent > coins) return i
    }
    return costs.length
}
export default maxBars
