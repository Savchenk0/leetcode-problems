export function addTwoNumbers(l1, l2) {
    const firstNum = +l1.reverse().join('')
    const secondNum = +l2.reverse().join('')
    const total = (firstNum + secondNum).toString()
    const result = []
    for (let i = total.length - 1; i >= 0; i--) {
        result.push(+total[i])
    }
    return result
}
