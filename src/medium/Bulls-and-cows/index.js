export function bullsAndCows(secret) {
    return function (guess) {
        if (guess === secret) return 'You guessed it right!'
        let bulls = 0
        let cows = 0
        const guessString = String(guess)
        const secretString = String(secret)
        const candidates = {}
        for (let i = 0; i < guessString.length; i++) {
            candidates[secretString[i]]
                ? candidates[secretString[i]]++
                : (candidates[secretString[i]] = 1)
        }
        for (let i = 0; i < guessString.length; i++) {
            if (guessString[i] === secretString[i]) {
                candidates[guessString[i]]--
                bulls++
            } else if (candidates[guessString[i]]) {
                cows++
                candidates[guessString[i]]--
            }
        }
        return `${bulls}A${cows}B`
    }
}
