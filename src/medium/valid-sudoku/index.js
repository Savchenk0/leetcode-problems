export default function isValidSudoku(board) {
    const rows = {}
    const columns = {}
    const sets = {}
    for (let i = 0; i < board.length; i++) {
        rows[i] = {}
        columns[i] = {}
        for (let j = 0; j < board[i].length; j++) {
            if (rows[i][board[i][j]] && rows[i][board[i][j]] !== '.')
                return false
            if (columns[i][board[j][i]] && columns[i][board[j][i]] !== '.')
                return false

            if (i < 3) {
                if (j < 3) {
                    if (!sets[1]) sets[1] = {}
                    if (sets[1][board[i][j]] && sets[1][board[i][j]] !== '.')
                        return false
                    sets[1][board[i][j]] = board[i][j]
                }
                if (j > 2 && j < 6) {
                    if (!sets[2]) sets[2] = {}
                    if (sets[2][board[i][j]] && sets[2][board[i][j]] !== '.')
                        return false
                    sets[2][board[i][j]] = board[i][j]
                }
                if (j > 5) {
                    if (!sets[3]) sets[3] = {}
                    if (sets[3][board[i][j]] && sets[3][board[i][j]] !== '.')
                        return false
                    sets[3][board[i][j]] = board[i][j]
                }
                if (i > 2 && i < 6) {
                    if (j < 3) {
                        if (!sets[4]) sets[4] = {}
                        if (
                            sets[4][board[i][j]] &&
                            sets[4][board[i][j]] !== '.'
                        )
                            return false
                        sets[4][board[i][j]] = board[i][j]
                    }
                    if (j > 2 && j < 6) {
                        if (!sets[5]) sets[5] = {}
                        if (
                            sets[5][board[i][j]] &&
                            sets[5][board[i][j]] !== '.'
                        )
                            return false
                        sets[5][board[i][j]] = board[i][j]
                    }
                    if (j > 5) {
                        if (!sets[6]) sets[6] = {}
                        if (
                            sets[6][board[i][j]] &&
                            sets[6][board[i][j]] !== '.'
                        )
                            return false
                        sets[6][board[i][j]] = board[i][j]
                    }
                }
                if (i > 5) {
                    if (j < 3) {
                        if (!sets[7]) sets[7] = {}
                        if (
                            sets[7][board[i][j]] &&
                            sets[7][board[i][j]] !== '.'
                        )
                            return false
                        sets[7][board[i][j]] = board[i][j]
                    }
                    if (j > 2 && j < 6) {
                        if (!sets[8]) sets[8] = {}
                        if (
                            sets[8][board[i][j]] &&
                            sets[8][board[i][j]] !== '.'
                        )
                            return false
                        sets[8][board[i][j]] = board[i][j]
                    }
                    if (j > 5) {
                        if (!sets[9]) sets[9] = {}
                        if (
                            sets[9][board[i][j]] &&
                            sets[9][board[i][j]] !== '.'
                        )
                            return false
                        sets[9][board[i][j]] = board[i][j]
                    }
                }
            }
            rows[i][board[i][j]] = board[i][j]
            columns[i][board[j][i]] = board[j][i]
        }
    }
    return true
}
