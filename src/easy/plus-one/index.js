const plusOne = (digits) => String(Number(digits.join('')) + 1).split('')

export default plusOne
