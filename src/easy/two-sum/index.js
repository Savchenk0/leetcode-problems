export const twoSum = (candidates, sum) => {
    const pool = {}

    for (const key in candidates) {
        if (pool[candidates[key]]) {
            return [+pool[candidates[key]], +key]
        } else {
            pool[sum - candidates[key]] = key
        }
    }

    return 'not found'
}
function sortedArraysSum(arr1, arr2, target, source, result, additionsValues) {
    // helper function
    for (let i = 0; i < arr1.length; i++) {
        for (let j = 0; j < arr2.length; j++) {
            if (arr1[i] + arr2[j] === target) {
                additionsValues.push(arr1[i])
                additionsValues.push(arr2[j])
                break
            }
            if (
                Math.abs(arr1[i] + arr2[j]) > Math.abs(target) &&
                target !== 0
            ) {
                break
            }
        }
        if (additionsValues.length === 2) {
            for (let j = 0; j < source.length; j++) {
                if (additionsValues.includes(source[j])) {
                    result.push(j)
                    additionsValues.splice(
                        additionsValues.indexOf(source[j]),
                        1
                    )
                }
                if (result.length === 2) return result
            }
        }
    }
}

export function twoSum2(nums, target) {
    const result = []
    const additionsValues = []

    if (target === 0) {
        const zeros = nums.filter((el) => el === 0)
        if (zeros.length > 1) {
            for (let i = 0; i < nums.length; i++) {
                if (nums[i] === target) result.push(i)
                if (result.length === 2) {
                    return result
                }
            }
        }
        const biggerThanTarget = nums
            .filter((el) => {
                return el > target
            })
            .sort((a, b) => {
                return a - b
            })

        const lowerThanTarget = nums
            .filter((el) => {
                return el < target
            })
            .sort((a, b) => {
                return b - a
            })

        const possibleCase = sortedArraysSum(
            lowerThanTarget,
            biggerThanTarget,
            target,
            nums,
            result,
            additionsValues
        )
        if (possibleCase) {
            return possibleCase
        }
    }

    if (target !== 0 && target % 2 === 0) {
        for (let i = 0; i < nums.length; i++) {
            if (nums[i] === target / 2) {
                result.push(i)
                if (result.length === 2) return result
            }
        }
        result.splice(0)
    }

    if (target > 0) {
        const biggerThanTarget = nums
            .filter((el) => {
                return el > target
            })
            .sort((a, b) => {
                return a - b
            })
        const negatives = nums
            .filter((el) => {
                return el < 0
            })
            .sort((a, b) => {
                return b - a
            })

        const possibleCase = sortedArraysSum(
            negatives,
            biggerThanTarget,
            target,
            nums,
            result,
            additionsValues
        )
        if (possibleCase) {
            return possibleCase
        }

        const nonNegativeLowerHalf = nums
            .filter((el) => {
                return el >= 0 && el < target / 2
            })
            .sort((a, b) => {
                return a - b
            })
        const nonNegativeHigherHalf = nums
            .filter((el) => {
                return el <= target && el > target / 2
            })
            .sort((a, b) => {
                return b - a
            })

        const possibleCase2 = sortedArraysSum(
            nonNegativeHigherHalf,
            nonNegativeLowerHalf,
            target,
            nums,
            result,
            additionsValues
        )
        if (possibleCase2) {
            return possibleCase2
        }
    }
    const lowerThanTarget = nums
        .filter((el) => {
            return el < target
        })
        .sort((a, b) => b - a)
    const positives = nums.filter((el) => {
        return el > 0
    })
    const possibleCase = sortedArraysSum(
        lowerThanTarget,
        positives,
        target,
        nums,
        result,
        additionsValues
    )
    if (possibleCase) {
        return possibleCase
    }

    const nonPositiveLowerHalf = nums
        .filter((el) => {
            return el <= 0 && el > target / 2
        })
        .sort((a, b) => {
            return b - a
        })
    const nonPositiveHigherHalf = nums
        .filter((el) => {
            return el < target / 2 && el >= target
        })
        .sort((a, b) => {
            return a - b
        })
    const possibleCase2 = sortedArraysSum(
        nonPositiveHigherHalf,
        nonPositiveLowerHalf,
        target,
        nums,
        result,
        additionsValues
    )
    if (possibleCase2) {
        return possibleCase2
    }
}
export function twoSum3(nums, target) {
    let counter = 0
    for (let i = 0; i < nums.length - 1; i++) {
        for (let j = i + 1; j < nums.length; j++) {
            counter++
            if (nums[i] + nums[j] === target) {
                return [i, j]
            }
        }
    }
    return { target, arrayLength: nums.length, n: counter / nums.length }
}
