export function isPalindrome(number) {
    const strNumber = number.toString()
    for (let i = 0; i < strNumber.length / 2; i++) {
        console.log(`iteration ${i + 1} :${strNumber[i]} ${strNumber[i + 1]}`)
        if (strNumber[i] !== strNumber[strNumber.length - 1 - i]) return false
    }
    return true
}
