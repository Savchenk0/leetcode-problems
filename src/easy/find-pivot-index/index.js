export const pivotIndex = function (nums) {
    let sumRight =
            nums.reduce((prev, curr) => {
                return prev + curr
            }, 0) - nums[0],
        sumLeft = 0

    for (let i = 0; i < nums.length; i++) {
        if (sumLeft === sumRight) return i
        sumLeft += nums[i]
        sumRight -= nums[i + 1]
    }
    return -1
}

export const pivotIndex2 = function (nums) {
    let sumRight = 0,
        sumLeft = 0
    for (let i = 1; i < nums.length; i++) {
        sumRight += nums[i]
    }
    for (let i = 0; i < nums.length; i++) {
        if (sumLeft === sumRight) return i
        sumLeft += nums[i]
        sumRight -= nums[i + 1]
    }
    return -1
}
