function factorial(number) {
    if (number === 0) return 1
    if (number === 1) return number
    return number * factorial(number - 1)
}
export default function atMostNGivenDigitSet(digits, number) {
    const digitsAmount = String(number).length
    const firstDigit = Number(String(number)[0])
    let numbersAmount = 0
    for (let i = 1; i < digitsAmount; i++) {
        numbersAmount += Math.pow(digits.length, i)
    }
    let sameDigitStarters = 0
    for (let i = 0; i < digits.length; i++) {
        if (digits[i] >= firstDigit) break
        sameDigitStarters++
    }
    numbersAmount += sameDigitStarters * factorial(digits.length - 1)
    if (
        String(number).length === digits.length &&
        digits.some(() => firstDigit)
    ) {
        let stringNum = String(number)
        let options = 1
        const digitsNum = digits.map((el) => Number(el))
        for (let i = 1; i <= stringNum.length; i++) {
            if (!digitsNum.some((el) => el <= Number(stringNum[i]))) {
                options = 0
                break
            }
            options *= digitsNum.reduce((acc, el) => {
                if (el <= Number(stringNum)[i]) {
                    return acc++
                }
                return acc
            }, 0)
        }
        numbersAmount += options
    }
    return numbersAmount
}
