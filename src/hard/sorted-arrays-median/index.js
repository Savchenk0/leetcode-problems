export const findMedianSortedArrays = function (nums1, nums2) {
    const result = []
    const mergedLength =
        (nums1.length + nums2.length) % 2
            ? Math.ceil((nums1.length + nums2.length) / 2)
            : (nums1.length + nums2.length) / 2 + 1
    for (let i = 0, y = 0; i + y < mergedLength; ) {
        if (i === nums1.length) {
            result.push(nums2[y])
            y++
            continue
        }
        if (y === nums2.length) {
            result.push(nums1[i])
            i++
            continue
        }
        if (nums1[i] < nums2[y]) {
            result.push(nums1[i])
            i++
        } else {
            result.push(nums2[y])
            y++
        }
    }

    if ((nums1.length + nums2.length) % 2) return result[mergedLength - 1]
    return (result[mergedLength - 1] + result[mergedLength - 2]) / 2
}
